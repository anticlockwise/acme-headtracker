﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace ros
{
    public class UnityConfig
    {
        public const string kLocalFrame = "/hangar_03";
        public const float kMetersPerUnityUnit = 1.0f;
    }

    public class HeadTracker : MonoBehaviour
    {
        PeopleClient peopleClient;

        void Start() {
            peopleClient = new PeopleClient("/hud/cameras/gaze/people");
            peopleClient.Start();
        }

        void OnApplicationQuit() {
            peopleClient.Terminate();
        }

        void Update() {
            if (peopleClient != null) {
                List<Person> people;
                if (peopleClient.Get(out people)) {
                    if (people.Count > 0) {
                        Person firstPerson = people[0];
                        Camera.main.transform.position = firstPerson.headPosition;
                    }
                }
            }
        }
    }
}
