﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ros_proto.people_tracking;

namespace ros
{
    public class Person
    {
        public Vector3 headPosition;
    }

    public class PeopleClient
      : RosProtoClient<PersonHypothesisArray,
      PersonHypothesisArray.Builder,
      List<Person>>
    {
        public PeopleClient(string topic)
            : base(topic, "people_tracking/PersonHypothesisArray") {
        }

        protected override List<Person> processMessage(PersonHypothesisArray msg) {
            List<Person> ret = new List<Person>();
            foreach (PersonHypothesis person in msg.PeopleList) {
                Person p = new Person();
                p.headPosition = new Vector3((float)person.HeadPose.Position.X,
                    (float)person.HeadPose.Position.Y,
                    (float)person.HeadPose.Position.Z);              
                ret.Add(p);
            }
            return ret;
        }
    }
}