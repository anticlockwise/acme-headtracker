// Generated by ProtoGen, Version=2.4.1.521, Culture=neutral, PublicKeyToken=55f7125234beb589.  DO NOT EDIT!
#pragma warning disable 1591, 0612, 3021
#region Designer generated code

using pb = global::Google.ProtocolBuffers;
using pbc = global::Google.ProtocolBuffers.Collections;
using pbd = global::Google.ProtocolBuffers.Descriptors;
using scg = global::System.Collections.Generic;
namespace ros_proto.sensor_msgs {
  
  namespace Proto {
    
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    public static partial class LaserScan {
    
      #region Extension registration
      public static void RegisterAllExtensions(pb::ExtensionRegistry registry) {
      }
      #endregion
      #region Static variables
      #endregion
      #region Extensions
      internal static readonly object Descriptor;
      static LaserScan() {
        Descriptor = null;
      }
      #endregion
      
    }
  }
  #region Messages
  [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
  public sealed partial class LaserScan : pb::GeneratedMessageLite<LaserScan, LaserScan.Builder> {
    private LaserScan() { }
    private static readonly LaserScan defaultInstance = new LaserScan().MakeReadOnly();
    private static readonly string[] _laserScanFieldNames = new string[] { "angle_increment", "angle_max", "angle_min", "header", "intensities", "range_max", "range_min", "ranges", "scan_time", "time_increment" };
    private static readonly uint[] _laserScanFieldTags = new uint[] { 37, 29, 21, 10, 85, 69, 61, 77, 53, 45 };
    public static LaserScan DefaultInstance {
      get { return defaultInstance; }
    }
    
    public override LaserScan DefaultInstanceForType {
      get { return DefaultInstance; }
    }
    
    protected override LaserScan ThisMessage {
      get { return this; }
    }
    
    public const int HeaderFieldNumber = 1;
    private bool hasHeader;
    private global::ros_proto.std_msgs.Header header_;
    public bool HasHeader {
      get { return hasHeader; }
    }
    public global::ros_proto.std_msgs.Header Header {
      get { return header_ ?? global::ros_proto.std_msgs.Header.DefaultInstance; }
    }
    
    public const int AngleMinFieldNumber = 2;
    private bool hasAngleMin;
    private float angleMin_;
    public bool HasAngleMin {
      get { return hasAngleMin; }
    }
    public float AngleMin {
      get { return angleMin_; }
    }
    
    public const int AngleMaxFieldNumber = 3;
    private bool hasAngleMax;
    private float angleMax_;
    public bool HasAngleMax {
      get { return hasAngleMax; }
    }
    public float AngleMax {
      get { return angleMax_; }
    }
    
    public const int AngleIncrementFieldNumber = 4;
    private bool hasAngleIncrement;
    private float angleIncrement_;
    public bool HasAngleIncrement {
      get { return hasAngleIncrement; }
    }
    public float AngleIncrement {
      get { return angleIncrement_; }
    }
    
    public const int TimeIncrementFieldNumber = 5;
    private bool hasTimeIncrement;
    private float timeIncrement_;
    public bool HasTimeIncrement {
      get { return hasTimeIncrement; }
    }
    public float TimeIncrement {
      get { return timeIncrement_; }
    }
    
    public const int ScanTimeFieldNumber = 6;
    private bool hasScanTime;
    private float scanTime_;
    public bool HasScanTime {
      get { return hasScanTime; }
    }
    public float ScanTime {
      get { return scanTime_; }
    }
    
    public const int RangeMinFieldNumber = 7;
    private bool hasRangeMin;
    private float rangeMin_;
    public bool HasRangeMin {
      get { return hasRangeMin; }
    }
    public float RangeMin {
      get { return rangeMin_; }
    }
    
    public const int RangeMaxFieldNumber = 8;
    private bool hasRangeMax;
    private float rangeMax_;
    public bool HasRangeMax {
      get { return hasRangeMax; }
    }
    public float RangeMax {
      get { return rangeMax_; }
    }
    
    public const int RangesFieldNumber = 9;
    private pbc::PopsicleList<float> ranges_ = new pbc::PopsicleList<float>();
    public scg::IList<float> RangesList {
      get { return pbc::Lists.AsReadOnly(ranges_); }
    }
    public int RangesCount {
      get { return ranges_.Count; }
    }
    public float GetRanges(int index) {
      return ranges_[index];
    }
    
    public const int IntensitiesFieldNumber = 10;
    private pbc::PopsicleList<float> intensities_ = new pbc::PopsicleList<float>();
    public scg::IList<float> IntensitiesList {
      get { return pbc::Lists.AsReadOnly(intensities_); }
    }
    public int IntensitiesCount {
      get { return intensities_.Count; }
    }
    public float GetIntensities(int index) {
      return intensities_[index];
    }
    
    public override bool IsInitialized {
      get {
        return true;
      }
    }
    
    public override void WriteTo(pb::ICodedOutputStream output) {
      int size = SerializedSize; size=size+0;
      string[] field_names = _laserScanFieldNames;
      if (hasHeader) {
        output.WriteMessage(1, field_names[3], Header);
      }
      if (hasAngleMin) {
        output.WriteFloat(2, field_names[2], AngleMin);
      }
      if (hasAngleMax) {
        output.WriteFloat(3, field_names[1], AngleMax);
      }
      if (hasAngleIncrement) {
        output.WriteFloat(4, field_names[0], AngleIncrement);
      }
      if (hasTimeIncrement) {
        output.WriteFloat(5, field_names[9], TimeIncrement);
      }
      if (hasScanTime) {
        output.WriteFloat(6, field_names[8], ScanTime);
      }
      if (hasRangeMin) {
        output.WriteFloat(7, field_names[6], RangeMin);
      }
      if (hasRangeMax) {
        output.WriteFloat(8, field_names[5], RangeMax);
      }
      if (ranges_.Count > 0) {
        output.WriteFloatArray(9, field_names[7], ranges_);
      }
      if (intensities_.Count > 0) {
        output.WriteFloatArray(10, field_names[4], intensities_);
      }
    }
    
    private int memoizedSerializedSize = -1;
    public override int SerializedSize {
      get {
        int size = memoizedSerializedSize;
        if (size != -1) return size;
        
        size = 0;
        if (hasHeader) {
          size += pb::CodedOutputStream.ComputeMessageSize(1, Header);
        }
        if (hasAngleMin) {
          size += pb::CodedOutputStream.ComputeFloatSize(2, AngleMin);
        }
        if (hasAngleMax) {
          size += pb::CodedOutputStream.ComputeFloatSize(3, AngleMax);
        }
        if (hasAngleIncrement) {
          size += pb::CodedOutputStream.ComputeFloatSize(4, AngleIncrement);
        }
        if (hasTimeIncrement) {
          size += pb::CodedOutputStream.ComputeFloatSize(5, TimeIncrement);
        }
        if (hasScanTime) {
          size += pb::CodedOutputStream.ComputeFloatSize(6, ScanTime);
        }
        if (hasRangeMin) {
          size += pb::CodedOutputStream.ComputeFloatSize(7, RangeMin);
        }
        if (hasRangeMax) {
          size += pb::CodedOutputStream.ComputeFloatSize(8, RangeMax);
        }
        {
          int dataSize = 0;
          dataSize = 4 * ranges_.Count;
          size += dataSize;
          size += 1 * ranges_.Count;
        }
        {
          int dataSize = 0;
          dataSize = 4 * intensities_.Count;
          size += dataSize;
          size += 1 * intensities_.Count;
        }
        memoizedSerializedSize = size;
        return size;
      }
    }
    
    #region Lite runtime methods
    public override int GetHashCode() {
      int hash = GetType().GetHashCode();
      if (hasHeader) hash ^= header_.GetHashCode();
      if (hasAngleMin) hash ^= angleMin_.GetHashCode();
      if (hasAngleMax) hash ^= angleMax_.GetHashCode();
      if (hasAngleIncrement) hash ^= angleIncrement_.GetHashCode();
      if (hasTimeIncrement) hash ^= timeIncrement_.GetHashCode();
      if (hasScanTime) hash ^= scanTime_.GetHashCode();
      if (hasRangeMin) hash ^= rangeMin_.GetHashCode();
      if (hasRangeMax) hash ^= rangeMax_.GetHashCode();
      foreach(float i in ranges_)
        hash ^= i.GetHashCode();
      foreach(float i in intensities_)
        hash ^= i.GetHashCode();
      return hash;
    }
    
    public override bool Equals(object obj) {
      LaserScan other = obj as LaserScan;
      if (other == null) return false;
      if (hasHeader != other.hasHeader || (hasHeader && !header_.Equals(other.header_))) return false;
      if (hasAngleMin != other.hasAngleMin || (hasAngleMin && !angleMin_.Equals(other.angleMin_))) return false;
      if (hasAngleMax != other.hasAngleMax || (hasAngleMax && !angleMax_.Equals(other.angleMax_))) return false;
      if (hasAngleIncrement != other.hasAngleIncrement || (hasAngleIncrement && !angleIncrement_.Equals(other.angleIncrement_))) return false;
      if (hasTimeIncrement != other.hasTimeIncrement || (hasTimeIncrement && !timeIncrement_.Equals(other.timeIncrement_))) return false;
      if (hasScanTime != other.hasScanTime || (hasScanTime && !scanTime_.Equals(other.scanTime_))) return false;
      if (hasRangeMin != other.hasRangeMin || (hasRangeMin && !rangeMin_.Equals(other.rangeMin_))) return false;
      if (hasRangeMax != other.hasRangeMax || (hasRangeMax && !rangeMax_.Equals(other.rangeMax_))) return false;
      if(ranges_.Count != other.ranges_.Count) return false;
      for(int ix=0; ix < ranges_.Count; ix++)
        if(!ranges_[ix].Equals(other.ranges_[ix])) return false;
      if(intensities_.Count != other.intensities_.Count) return false;
      for(int ix=0; ix < intensities_.Count; ix++)
        if(!intensities_[ix].Equals(other.intensities_[ix])) return false;
      return true;
    }
    
    public override void PrintTo(global::System.IO.TextWriter writer) {
      PrintField("header", hasHeader, header_, writer);
      PrintField("angle_min", hasAngleMin, angleMin_, writer);
      PrintField("angle_max", hasAngleMax, angleMax_, writer);
      PrintField("angle_increment", hasAngleIncrement, angleIncrement_, writer);
      PrintField("time_increment", hasTimeIncrement, timeIncrement_, writer);
      PrintField("scan_time", hasScanTime, scanTime_, writer);
      PrintField("range_min", hasRangeMin, rangeMin_, writer);
      PrintField("range_max", hasRangeMax, rangeMax_, writer);
      PrintField("ranges", ranges_, writer);
      PrintField("intensities", intensities_, writer);
    }
    #endregion
    
    public static LaserScan ParseFrom(pb::ByteString data) {
      return ((Builder) CreateBuilder().MergeFrom(data)).BuildParsed();
    }
    public static LaserScan ParseFrom(pb::ByteString data, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(data, extensionRegistry)).BuildParsed();
    }
    public static LaserScan ParseFrom(byte[] data) {
      return ((Builder) CreateBuilder().MergeFrom(data)).BuildParsed();
    }
    public static LaserScan ParseFrom(byte[] data, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(data, extensionRegistry)).BuildParsed();
    }
    public static LaserScan ParseFrom(global::System.IO.Stream input) {
      return ((Builder) CreateBuilder().MergeFrom(input)).BuildParsed();
    }
    public static LaserScan ParseFrom(global::System.IO.Stream input, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(input, extensionRegistry)).BuildParsed();
    }
    public static LaserScan ParseDelimitedFrom(global::System.IO.Stream input) {
      return CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }
    public static LaserScan ParseDelimitedFrom(global::System.IO.Stream input, pb::ExtensionRegistry extensionRegistry) {
      return CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }
    public static LaserScan ParseFrom(pb::ICodedInputStream input) {
      return ((Builder) CreateBuilder().MergeFrom(input)).BuildParsed();
    }
    public static LaserScan ParseFrom(pb::ICodedInputStream input, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(input, extensionRegistry)).BuildParsed();
    }
    private LaserScan MakeReadOnly() {
      ranges_.MakeReadOnly();
      intensities_.MakeReadOnly();
      return this;
    }
    
    public static Builder CreateBuilder() { return new Builder(); }
    public override Builder ToBuilder() { return CreateBuilder(this); }
    public override Builder CreateBuilderForType() { return new Builder(); }
    public static Builder CreateBuilder(LaserScan prototype) {
      return new Builder(prototype);
    }
    
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    public sealed partial class Builder : pb::GeneratedBuilderLite<LaserScan, Builder> {
      protected override Builder ThisBuilder {
        get { return this; }
      }
      public Builder() {
        result = DefaultInstance;
        resultIsReadOnly = true;
      }
      internal Builder(LaserScan cloneFrom) {
        result = cloneFrom;
        resultIsReadOnly = true;
      }
      
      private bool resultIsReadOnly;
      private LaserScan result;
      
      private LaserScan PrepareBuilder() {
        if (resultIsReadOnly) {
          LaserScan original = result;
          result = new LaserScan();
          resultIsReadOnly = false;
          MergeFrom(original);
        }
        return result;
      }
      
      public override bool IsInitialized {
        get { return result.IsInitialized; }
      }
      
      protected override LaserScan MessageBeingBuilt {
        get { return PrepareBuilder(); }
      }
      
      public override Builder Clear() {
        result = DefaultInstance;
        resultIsReadOnly = true;
        return this;
      }
      
      public override Builder Clone() {
        if (resultIsReadOnly) {
          return new Builder(result);
        } else {
          return new Builder().MergeFrom(result);
        }
      }
      
      public override LaserScan DefaultInstanceForType {
        get { return global::ros_proto.sensor_msgs.LaserScan.DefaultInstance; }
      }
      
      public override LaserScan BuildPartial() {
        if (resultIsReadOnly) {
          return result;
        }
        resultIsReadOnly = true;
        return result.MakeReadOnly();
      }
      
      public override Builder MergeFrom(pb::IMessageLite other) {
        if (other is LaserScan) {
          return MergeFrom((LaserScan) other);
        } else {
          base.MergeFrom(other);
          return this;
        }
      }
      
      public override Builder MergeFrom(LaserScan other) {
        if (other == global::ros_proto.sensor_msgs.LaserScan.DefaultInstance) return this;
        PrepareBuilder();
        if (other.HasHeader) {
          MergeHeader(other.Header);
        }
        if (other.HasAngleMin) {
          AngleMin = other.AngleMin;
        }
        if (other.HasAngleMax) {
          AngleMax = other.AngleMax;
        }
        if (other.HasAngleIncrement) {
          AngleIncrement = other.AngleIncrement;
        }
        if (other.HasTimeIncrement) {
          TimeIncrement = other.TimeIncrement;
        }
        if (other.HasScanTime) {
          ScanTime = other.ScanTime;
        }
        if (other.HasRangeMin) {
          RangeMin = other.RangeMin;
        }
        if (other.HasRangeMax) {
          RangeMax = other.RangeMax;
        }
        if (other.ranges_.Count != 0) {
          result.ranges_.Add(other.ranges_);
        }
        if (other.intensities_.Count != 0) {
          result.intensities_.Add(other.intensities_);
        }
        return this;
      }
      
      public override Builder MergeFrom(pb::ICodedInputStream input) {
        return MergeFrom(input, pb::ExtensionRegistry.Empty);
      }
      
      public override Builder MergeFrom(pb::ICodedInputStream input, pb::ExtensionRegistry extensionRegistry) {
        PrepareBuilder();
        uint tag;
        string field_name;
        while (input.ReadTag(out tag, out field_name)) {
          if(tag == 0 && field_name != null) {
            int field_ordinal = global::System.Array.BinarySearch(_laserScanFieldNames, field_name, global::System.StringComparer.Ordinal);
            if(field_ordinal >= 0)
              tag = _laserScanFieldTags[field_ordinal];
            else {
              ParseUnknownField(input, extensionRegistry, tag, field_name);
              continue;
            }
          }
          switch (tag) {
            case 0: {
              throw pb::InvalidProtocolBufferException.InvalidTag();
            }
            default: {
              if (pb::WireFormat.IsEndGroupTag(tag)) {
                return this;
              }
              ParseUnknownField(input, extensionRegistry, tag, field_name);
              break;
            }
            case 10: {
              global::ros_proto.std_msgs.Header.Builder subBuilder = global::ros_proto.std_msgs.Header.CreateBuilder();
              if (result.hasHeader) {
                subBuilder.MergeFrom(Header);
              }
              input.ReadMessage(subBuilder, extensionRegistry);
              Header = subBuilder.BuildPartial();
              break;
            }
            case 21: {
              result.hasAngleMin = input.ReadFloat(ref result.angleMin_);
              break;
            }
            case 29: {
              result.hasAngleMax = input.ReadFloat(ref result.angleMax_);
              break;
            }
            case 37: {
              result.hasAngleIncrement = input.ReadFloat(ref result.angleIncrement_);
              break;
            }
            case 45: {
              result.hasTimeIncrement = input.ReadFloat(ref result.timeIncrement_);
              break;
            }
            case 53: {
              result.hasScanTime = input.ReadFloat(ref result.scanTime_);
              break;
            }
            case 61: {
              result.hasRangeMin = input.ReadFloat(ref result.rangeMin_);
              break;
            }
            case 69: {
              result.hasRangeMax = input.ReadFloat(ref result.rangeMax_);
              break;
            }
            case 74:
            case 77: {
              input.ReadFloatArray(tag, field_name, result.ranges_);
              break;
            }
            case 82:
            case 85: {
              input.ReadFloatArray(tag, field_name, result.intensities_);
              break;
            }
          }
        }
        
        return this;
      }
      
      
      public bool HasHeader {
       get { return result.hasHeader; }
      }
      public global::ros_proto.std_msgs.Header Header {
        get { return result.Header; }
        set { SetHeader(value); }
      }
      public Builder SetHeader(global::ros_proto.std_msgs.Header value) {
        pb::ThrowHelper.ThrowIfNull(value, "value");
        PrepareBuilder();
        result.hasHeader = true;
        result.header_ = value;
        return this;
      }
      public Builder SetHeader(global::ros_proto.std_msgs.Header.Builder builderForValue) {
        pb::ThrowHelper.ThrowIfNull(builderForValue, "builderForValue");
        PrepareBuilder();
        result.hasHeader = true;
        result.header_ = builderForValue.Build();
        return this;
      }
      public Builder MergeHeader(global::ros_proto.std_msgs.Header value) {
        pb::ThrowHelper.ThrowIfNull(value, "value");
        PrepareBuilder();
        if (result.hasHeader &&
            result.header_ != global::ros_proto.std_msgs.Header.DefaultInstance) {
            result.header_ = global::ros_proto.std_msgs.Header.CreateBuilder(result.header_).MergeFrom(value).BuildPartial();
        } else {
          result.header_ = value;
        }
        result.hasHeader = true;
        return this;
      }
      public Builder ClearHeader() {
        PrepareBuilder();
        result.hasHeader = false;
        result.header_ = null;
        return this;
      }
      
      public bool HasAngleMin {
        get { return result.hasAngleMin; }
      }
      public float AngleMin {
        get { return result.AngleMin; }
        set { SetAngleMin(value); }
      }
      public Builder SetAngleMin(float value) {
        PrepareBuilder();
        result.hasAngleMin = true;
        result.angleMin_ = value;
        return this;
      }
      public Builder ClearAngleMin() {
        PrepareBuilder();
        result.hasAngleMin = false;
        result.angleMin_ = 0F;
        return this;
      }
      
      public bool HasAngleMax {
        get { return result.hasAngleMax; }
      }
      public float AngleMax {
        get { return result.AngleMax; }
        set { SetAngleMax(value); }
      }
      public Builder SetAngleMax(float value) {
        PrepareBuilder();
        result.hasAngleMax = true;
        result.angleMax_ = value;
        return this;
      }
      public Builder ClearAngleMax() {
        PrepareBuilder();
        result.hasAngleMax = false;
        result.angleMax_ = 0F;
        return this;
      }
      
      public bool HasAngleIncrement {
        get { return result.hasAngleIncrement; }
      }
      public float AngleIncrement {
        get { return result.AngleIncrement; }
        set { SetAngleIncrement(value); }
      }
      public Builder SetAngleIncrement(float value) {
        PrepareBuilder();
        result.hasAngleIncrement = true;
        result.angleIncrement_ = value;
        return this;
      }
      public Builder ClearAngleIncrement() {
        PrepareBuilder();
        result.hasAngleIncrement = false;
        result.angleIncrement_ = 0F;
        return this;
      }
      
      public bool HasTimeIncrement {
        get { return result.hasTimeIncrement; }
      }
      public float TimeIncrement {
        get { return result.TimeIncrement; }
        set { SetTimeIncrement(value); }
      }
      public Builder SetTimeIncrement(float value) {
        PrepareBuilder();
        result.hasTimeIncrement = true;
        result.timeIncrement_ = value;
        return this;
      }
      public Builder ClearTimeIncrement() {
        PrepareBuilder();
        result.hasTimeIncrement = false;
        result.timeIncrement_ = 0F;
        return this;
      }
      
      public bool HasScanTime {
        get { return result.hasScanTime; }
      }
      public float ScanTime {
        get { return result.ScanTime; }
        set { SetScanTime(value); }
      }
      public Builder SetScanTime(float value) {
        PrepareBuilder();
        result.hasScanTime = true;
        result.scanTime_ = value;
        return this;
      }
      public Builder ClearScanTime() {
        PrepareBuilder();
        result.hasScanTime = false;
        result.scanTime_ = 0F;
        return this;
      }
      
      public bool HasRangeMin {
        get { return result.hasRangeMin; }
      }
      public float RangeMin {
        get { return result.RangeMin; }
        set { SetRangeMin(value); }
      }
      public Builder SetRangeMin(float value) {
        PrepareBuilder();
        result.hasRangeMin = true;
        result.rangeMin_ = value;
        return this;
      }
      public Builder ClearRangeMin() {
        PrepareBuilder();
        result.hasRangeMin = false;
        result.rangeMin_ = 0F;
        return this;
      }
      
      public bool HasRangeMax {
        get { return result.hasRangeMax; }
      }
      public float RangeMax {
        get { return result.RangeMax; }
        set { SetRangeMax(value); }
      }
      public Builder SetRangeMax(float value) {
        PrepareBuilder();
        result.hasRangeMax = true;
        result.rangeMax_ = value;
        return this;
      }
      public Builder ClearRangeMax() {
        PrepareBuilder();
        result.hasRangeMax = false;
        result.rangeMax_ = 0F;
        return this;
      }
      
      public pbc::IPopsicleList<float> RangesList {
        get { return PrepareBuilder().ranges_; }
      }
      public int RangesCount {
        get { return result.RangesCount; }
      }
      public float GetRanges(int index) {
        return result.GetRanges(index);
      }
      public Builder SetRanges(int index, float value) {
        PrepareBuilder();
        result.ranges_[index] = value;
        return this;
      }
      public Builder AddRanges(float value) {
        PrepareBuilder();
        result.ranges_.Add(value);
        return this;
      }
      public Builder AddRangeRanges(scg::IEnumerable<float> values) {
        PrepareBuilder();
        result.ranges_.Add(values);
        return this;
      }
      public Builder ClearRanges() {
        PrepareBuilder();
        result.ranges_.Clear();
        return this;
      }
      
      public pbc::IPopsicleList<float> IntensitiesList {
        get { return PrepareBuilder().intensities_; }
      }
      public int IntensitiesCount {
        get { return result.IntensitiesCount; }
      }
      public float GetIntensities(int index) {
        return result.GetIntensities(index);
      }
      public Builder SetIntensities(int index, float value) {
        PrepareBuilder();
        result.intensities_[index] = value;
        return this;
      }
      public Builder AddIntensities(float value) {
        PrepareBuilder();
        result.intensities_.Add(value);
        return this;
      }
      public Builder AddRangeIntensities(scg::IEnumerable<float> values) {
        PrepareBuilder();
        result.intensities_.Add(values);
        return this;
      }
      public Builder ClearIntensities() {
        PrepareBuilder();
        result.intensities_.Clear();
        return this;
      }
    }
    static LaserScan() {
      object.ReferenceEquals(global::ros_proto.sensor_msgs.Proto.LaserScan.Descriptor, null);
    }
  }
  
  #endregion
  
}

#endregion Designer generated code
