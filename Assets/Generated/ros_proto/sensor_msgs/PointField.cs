// Generated by ProtoGen, Version=2.4.1.521, Culture=neutral, PublicKeyToken=55f7125234beb589.  DO NOT EDIT!
#pragma warning disable 1591, 0612, 3021
#region Designer generated code

using pb = global::Google.ProtocolBuffers;
using pbc = global::Google.ProtocolBuffers.Collections;
using pbd = global::Google.ProtocolBuffers.Descriptors;
using scg = global::System.Collections.Generic;
namespace ros_proto.sensor_msgs {
  
  namespace Proto {
    
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    public static partial class PointField {
    
      #region Extension registration
      public static void RegisterAllExtensions(pb::ExtensionRegistry registry) {
      }
      #endregion
      #region Static variables
      #endregion
      #region Extensions
      internal static readonly object Descriptor;
      static PointField() {
        Descriptor = null;
      }
      #endregion
      
    }
  }
  #region Messages
  [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
  public sealed partial class PointField : pb::GeneratedMessageLite<PointField, PointField.Builder> {
    private PointField() { }
    private static readonly PointField defaultInstance = new PointField().MakeReadOnly();
    private static readonly string[] _pointFieldFieldNames = new string[] { "count", "datatype", "name", "offset" };
    private static readonly uint[] _pointFieldFieldTags = new uint[] { 32, 24, 10, 16 };
    public static PointField DefaultInstance {
      get { return defaultInstance; }
    }
    
    public override PointField DefaultInstanceForType {
      get { return DefaultInstance; }
    }
    
    protected override PointField ThisMessage {
      get { return this; }
    }
    
    #region Nested types
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    public static partial class Types {
      public enum Datatype {
        INT8 = 1,
        UINT8 = 2,
        INT16 = 3,
        UINT16 = 4,
        INT32 = 5,
        UINT32 = 6,
        FLOAT32 = 7,
        FLOAT64 = 8,
      }
      
    }
    #endregion
    
    public const int NameFieldNumber = 1;
    private bool hasName;
    private string name_ = "";
    public bool HasName {
      get { return hasName; }
    }
    public string Name {
      get { return name_; }
    }
    
    public const int OffsetFieldNumber = 2;
    private bool hasOffset;
    private uint offset_;
    public bool HasOffset {
      get { return hasOffset; }
    }
    [global::System.CLSCompliant(false)]
    public uint Offset {
      get { return offset_; }
    }
    
    public const int DatatypeFieldNumber = 3;
    private bool hasDatatype;
    private global::ros_proto.sensor_msgs.PointField.Types.Datatype datatype_ = global::ros_proto.sensor_msgs.PointField.Types.Datatype.INT8;
    public bool HasDatatype {
      get { return hasDatatype; }
    }
    public global::ros_proto.sensor_msgs.PointField.Types.Datatype Datatype {
      get { return datatype_; }
    }
    
    public const int CountFieldNumber = 4;
    private bool hasCount;
    private uint count_;
    public bool HasCount {
      get { return hasCount; }
    }
    [global::System.CLSCompliant(false)]
    public uint Count {
      get { return count_; }
    }
    
    public override bool IsInitialized {
      get {
        return true;
      }
    }
    
    public override void WriteTo(pb::ICodedOutputStream output) {
      int size = SerializedSize; size=size+0;
      string[] field_names = _pointFieldFieldNames;
      if (hasName) {
        output.WriteString(1, field_names[2], Name);
      }
      if (hasOffset) {
        output.WriteUInt32(2, field_names[3], Offset);
      }
      if (hasDatatype) {
        output.WriteEnum(3, field_names[1], (int) Datatype, Datatype);
      }
      if (hasCount) {
        output.WriteUInt32(4, field_names[0], Count);
      }
    }
    
    private int memoizedSerializedSize = -1;
    public override int SerializedSize {
      get {
        int size = memoizedSerializedSize;
        if (size != -1) return size;
        
        size = 0;
        if (hasName) {
          size += pb::CodedOutputStream.ComputeStringSize(1, Name);
        }
        if (hasOffset) {
          size += pb::CodedOutputStream.ComputeUInt32Size(2, Offset);
        }
        if (hasDatatype) {
          size += pb::CodedOutputStream.ComputeEnumSize(3, (int) Datatype);
        }
        if (hasCount) {
          size += pb::CodedOutputStream.ComputeUInt32Size(4, Count);
        }
        memoizedSerializedSize = size;
        return size;
      }
    }
    
    #region Lite runtime methods
    public override int GetHashCode() {
      int hash = GetType().GetHashCode();
      if (hasName) hash ^= name_.GetHashCode();
      if (hasOffset) hash ^= offset_.GetHashCode();
      if (hasDatatype) hash ^= datatype_.GetHashCode();
      if (hasCount) hash ^= count_.GetHashCode();
      return hash;
    }
    
    public override bool Equals(object obj) {
      PointField other = obj as PointField;
      if (other == null) return false;
      if (hasName != other.hasName || (hasName && !name_.Equals(other.name_))) return false;
      if (hasOffset != other.hasOffset || (hasOffset && !offset_.Equals(other.offset_))) return false;
      if (hasDatatype != other.hasDatatype || (hasDatatype && !datatype_.Equals(other.datatype_))) return false;
      if (hasCount != other.hasCount || (hasCount && !count_.Equals(other.count_))) return false;
      return true;
    }
    
    public override void PrintTo(global::System.IO.TextWriter writer) {
      PrintField("name", hasName, name_, writer);
      PrintField("offset", hasOffset, offset_, writer);
      PrintField("datatype", hasDatatype, datatype_, writer);
      PrintField("count", hasCount, count_, writer);
    }
    #endregion
    
    public static PointField ParseFrom(pb::ByteString data) {
      return ((Builder) CreateBuilder().MergeFrom(data)).BuildParsed();
    }
    public static PointField ParseFrom(pb::ByteString data, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(data, extensionRegistry)).BuildParsed();
    }
    public static PointField ParseFrom(byte[] data) {
      return ((Builder) CreateBuilder().MergeFrom(data)).BuildParsed();
    }
    public static PointField ParseFrom(byte[] data, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(data, extensionRegistry)).BuildParsed();
    }
    public static PointField ParseFrom(global::System.IO.Stream input) {
      return ((Builder) CreateBuilder().MergeFrom(input)).BuildParsed();
    }
    public static PointField ParseFrom(global::System.IO.Stream input, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(input, extensionRegistry)).BuildParsed();
    }
    public static PointField ParseDelimitedFrom(global::System.IO.Stream input) {
      return CreateBuilder().MergeDelimitedFrom(input).BuildParsed();
    }
    public static PointField ParseDelimitedFrom(global::System.IO.Stream input, pb::ExtensionRegistry extensionRegistry) {
      return CreateBuilder().MergeDelimitedFrom(input, extensionRegistry).BuildParsed();
    }
    public static PointField ParseFrom(pb::ICodedInputStream input) {
      return ((Builder) CreateBuilder().MergeFrom(input)).BuildParsed();
    }
    public static PointField ParseFrom(pb::ICodedInputStream input, pb::ExtensionRegistry extensionRegistry) {
      return ((Builder) CreateBuilder().MergeFrom(input, extensionRegistry)).BuildParsed();
    }
    private PointField MakeReadOnly() {
      return this;
    }
    
    public static Builder CreateBuilder() { return new Builder(); }
    public override Builder ToBuilder() { return CreateBuilder(this); }
    public override Builder CreateBuilderForType() { return new Builder(); }
    public static Builder CreateBuilder(PointField prototype) {
      return new Builder(prototype);
    }
    
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    public sealed partial class Builder : pb::GeneratedBuilderLite<PointField, Builder> {
      protected override Builder ThisBuilder {
        get { return this; }
      }
      public Builder() {
        result = DefaultInstance;
        resultIsReadOnly = true;
      }
      internal Builder(PointField cloneFrom) {
        result = cloneFrom;
        resultIsReadOnly = true;
      }
      
      private bool resultIsReadOnly;
      private PointField result;
      
      private PointField PrepareBuilder() {
        if (resultIsReadOnly) {
          PointField original = result;
          result = new PointField();
          resultIsReadOnly = false;
          MergeFrom(original);
        }
        return result;
      }
      
      public override bool IsInitialized {
        get { return result.IsInitialized; }
      }
      
      protected override PointField MessageBeingBuilt {
        get { return PrepareBuilder(); }
      }
      
      public override Builder Clear() {
        result = DefaultInstance;
        resultIsReadOnly = true;
        return this;
      }
      
      public override Builder Clone() {
        if (resultIsReadOnly) {
          return new Builder(result);
        } else {
          return new Builder().MergeFrom(result);
        }
      }
      
      public override PointField DefaultInstanceForType {
        get { return global::ros_proto.sensor_msgs.PointField.DefaultInstance; }
      }
      
      public override PointField BuildPartial() {
        if (resultIsReadOnly) {
          return result;
        }
        resultIsReadOnly = true;
        return result.MakeReadOnly();
      }
      
      public override Builder MergeFrom(pb::IMessageLite other) {
        if (other is PointField) {
          return MergeFrom((PointField) other);
        } else {
          base.MergeFrom(other);
          return this;
        }
      }
      
      public override Builder MergeFrom(PointField other) {
        if (other == global::ros_proto.sensor_msgs.PointField.DefaultInstance) return this;
        PrepareBuilder();
        if (other.HasName) {
          Name = other.Name;
        }
        if (other.HasOffset) {
          Offset = other.Offset;
        }
        if (other.HasDatatype) {
          Datatype = other.Datatype;
        }
        if (other.HasCount) {
          Count = other.Count;
        }
        return this;
      }
      
      public override Builder MergeFrom(pb::ICodedInputStream input) {
        return MergeFrom(input, pb::ExtensionRegistry.Empty);
      }
      
      public override Builder MergeFrom(pb::ICodedInputStream input, pb::ExtensionRegistry extensionRegistry) {
        PrepareBuilder();
        uint tag;
        string field_name;
        while (input.ReadTag(out tag, out field_name)) {
          if(tag == 0 && field_name != null) {
            int field_ordinal = global::System.Array.BinarySearch(_pointFieldFieldNames, field_name, global::System.StringComparer.Ordinal);
            if(field_ordinal >= 0)
              tag = _pointFieldFieldTags[field_ordinal];
            else {
              ParseUnknownField(input, extensionRegistry, tag, field_name);
              continue;
            }
          }
          switch (tag) {
            case 0: {
              throw pb::InvalidProtocolBufferException.InvalidTag();
            }
            default: {
              if (pb::WireFormat.IsEndGroupTag(tag)) {
                return this;
              }
              ParseUnknownField(input, extensionRegistry, tag, field_name);
              break;
            }
            case 10: {
              result.hasName = input.ReadString(ref result.name_);
              break;
            }
            case 16: {
              result.hasOffset = input.ReadUInt32(ref result.offset_);
              break;
            }
            case 24: {
              object unknown;
              if(input.ReadEnum(ref result.datatype_, out unknown)) {
                result.hasDatatype = true;
              } else if(unknown is int) {
              }
              break;
            }
            case 32: {
              result.hasCount = input.ReadUInt32(ref result.count_);
              break;
            }
          }
        }
        
        return this;
      }
      
      
      public bool HasName {
        get { return result.hasName; }
      }
      public string Name {
        get { return result.Name; }
        set { SetName(value); }
      }
      public Builder SetName(string value) {
        pb::ThrowHelper.ThrowIfNull(value, "value");
        PrepareBuilder();
        result.hasName = true;
        result.name_ = value;
        return this;
      }
      public Builder ClearName() {
        PrepareBuilder();
        result.hasName = false;
        result.name_ = "";
        return this;
      }
      
      public bool HasOffset {
        get { return result.hasOffset; }
      }
      [global::System.CLSCompliant(false)]
      public uint Offset {
        get { return result.Offset; }
        set { SetOffset(value); }
      }
      [global::System.CLSCompliant(false)]
      public Builder SetOffset(uint value) {
        PrepareBuilder();
        result.hasOffset = true;
        result.offset_ = value;
        return this;
      }
      public Builder ClearOffset() {
        PrepareBuilder();
        result.hasOffset = false;
        result.offset_ = 0;
        return this;
      }
      
      public bool HasDatatype {
       get { return result.hasDatatype; }
      }
      public global::ros_proto.sensor_msgs.PointField.Types.Datatype Datatype {
        get { return result.Datatype; }
        set { SetDatatype(value); }
      }
      public Builder SetDatatype(global::ros_proto.sensor_msgs.PointField.Types.Datatype value) {
        PrepareBuilder();
        result.hasDatatype = true;
        result.datatype_ = value;
        return this;
      }
      public Builder ClearDatatype() {
        PrepareBuilder();
        result.hasDatatype = false;
        result.datatype_ = global::ros_proto.sensor_msgs.PointField.Types.Datatype.INT8;
        return this;
      }
      
      public bool HasCount {
        get { return result.hasCount; }
      }
      [global::System.CLSCompliant(false)]
      public uint Count {
        get { return result.Count; }
        set { SetCount(value); }
      }
      [global::System.CLSCompliant(false)]
      public Builder SetCount(uint value) {
        PrepareBuilder();
        result.hasCount = true;
        result.count_ = value;
        return this;
      }
      public Builder ClearCount() {
        PrepareBuilder();
        result.hasCount = false;
        result.count_ = 0;
        return this;
      }
    }
    static PointField() {
      object.ReferenceEquals(global::ros_proto.sensor_msgs.Proto.PointField.Descriptor, null);
    }
  }
  
  #endregion
  
}

#endregion Designer generated code
