using UnityEngine;

namespace ros
{
using ros_proto.sensor_msgs;

public class RosImage {
  // Exactly one of AsColor or AsRaw will be set
  public Color32[] AsColor;
  public byte[] AsRaw;
  public int Width, Height;
  public RosImage() {
    AsColor = null;
    AsRaw = null;
    Width = Height = 1;
  }
}

// Subscribes to ros::Image topics
//
// Returns image as a byte array; it can only be turned into a texture
// on the render thread. Convenience methods to do this are provided.
//
public class ImageClient
  : RosProtoClient<Image, Image.Builder, RosImage>
{
  static string getTopicString(string topic, bool compressed) {
    if (topic==null) { return null; }
    if (compressed) { topic += "/compressed"; }
    return topic;
  }

  public ImageClient(string topic, bool compressed)
    : base(getTopicString(topic, compressed), "sensor_msgs/Image") {
  }

  protected override RosImage processMessage(Image msg) {
    if (msg.Encoding == "rgb8" ||
        msg.Encoding == "bgr8" ||
        msg.Encoding == "8UC3" ||
        msg.Encoding == "rgba8" ||
        msg.Encoding == "abgr8") {
      // These formats require a manual copy, to expand them to RGBA8
      int total = (int)(msg.Height * msg.Width);
      byte[] bytes = ToByteArray(msg.Data);
      Color32[] color = new Color32[total];
      // Not sure what 8UC3 channel mapping is, so guess!
      if (msg.Encoding == "rgb8" || msg.Encoding == "8UC3") {
        unchecked {
          for (int i = 0; i < total; ++i) {
            byte r = bytes[i*3 + 0];
            byte g = bytes[i*3 + 1];
            byte b = bytes[i*3 + 2];
            color[i] = new Color32(r,g,b,255);
          }
        }
      } else if (msg.Encoding == "bgr8") {
        unchecked {
          for (int i = 0; i < total; ++i) {
            byte b = bytes[i*3 + 0];
            byte g = bytes[i*3 + 1];
            byte r = bytes[i*3 + 2];
            color[i] = new Color32(r,g,b,255);
          }
        }
      } else if (msg.Encoding == "rgba8" || msg.Encoding == "abgr8") {
        // Direct copy: one of these is correct, one is incorrect.
        // But not sure which, because I can't find documentation on the
        // internal layout of Color32
        unsafe {
          fixed (Color32* colorPtr = color) {
            System.IntPtr colorIntptr = new System.IntPtr(colorPtr);
            System.Runtime.InteropServices.Marshal.Copy(
                bytes, 0, colorIntptr, total * sizeof(Color32));
          }
        }
      }

      RosImage img = new RosImage();
      img.Width = (int)msg.Width;
      img.Height = (int)msg.Height;
      img.AsColor = color;
      // img.AsRaw = bytes;
      return img;
    } else if (msg.Encoding == "jpeg" || msg.Encoding == "png") {
      // Compressed formats are passed along raw; it's assumed that the
      // consumer will be able to decode them.
      RosImage img = new RosImage();
      img.Width = (int)msg.Width;
      img.Height = (int)msg.Height;
      // img.AsColor = color;
      img.AsRaw = msg.Data.ToByteArray();  // I guess a copy is appropriate here
      return img;
    } else {
      Debug.Log(string.Format("Don't understand image format {0}", msg.Encoding));
      return null;
    }
  }

  // Internal helper method: extracts a byte[] from a protobuf without
  // doing an unnecessary copy.
  static System.Reflection.FieldInfo FIELD_ByteString_bytes =
    typeof(Google.ProtocolBuffers.ByteString).GetField("bytes",
             System.Reflection.BindingFlags.NonPublic |
             System.Reflection.BindingFlags.GetField |
             System.Reflection.BindingFlags.Instance);
  static byte[] ToByteArray(Google.ProtocolBuffers.ByteString bs) {
    return (byte[])FIELD_ByteString_bytes.GetValue(bs);
  }

  // This must be called on the main thread.
  // texs[] should start out null; will be mutated.
  static public void ApplyToMaterial(RosImage msg, Material mat, ref Texture2D[] texs) {
    // nb: double-buffering doesn't seem necessary yet? So just use one tex.
    if (texs == null) {
      texs = new Texture2D[1];
      for (int i = 0; i < 1; ++i) {
        texs[i] = new Texture2D(4, 4, TextureFormat.ARGB32, false);
      }
    }
    Texture2D tex = texs[0];

    if (msg.AsRaw != null) {
      // Decode a png/jpeg; this sets the size, etc.
      tex.LoadImage(msg.AsRaw);
    } else if (msg.AsColor != null) {
      if (tex.width != msg.Width || tex.height != msg.Height) {
        tex.Resize(msg.Width, msg.Height, TextureFormat.ARGB32, false);
      }
      tex.SetPixels32(msg.AsColor, 0);
    } else {
      Debug.Log("Cannot apply RosImage to material: unsupported format?");
      return;
    }

    mat.mainTexture = tex;
    tex.Apply();
  }
}

}  // namespace ros
