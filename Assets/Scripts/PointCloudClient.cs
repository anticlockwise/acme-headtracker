// Subscribes to ros::PointCloud2 topics
// 
// Emits PointsAndTransform instances, consisting of the PointCloud
// points, with padding removed, and split into chunks of at most N
// points.
// 
// These chunks are (not coincidentally) perfect for use as
// vertex buffers.

using UnityEngine;

namespace ros
{
using PointCloud2 = ros_proto.sensor_msgs.PointCloud2;

public class PointsAndTransform {
  public static PointsAndTransform GetEmpty() {
    PointsAndTransform ret = new PointsAndTransform();
    ret.transform = Matrix4x4.identity;
    ret.point_chunks = new Vector3[0][];
    return ret;
  }
  public Matrix4x4 transform;
  public Vector3[][] point_chunks;
}

public class PointCloudClient
  : RosProtoClient<PointCloud2, PointCloud2.Builder, PointsAndTransform>
{
  // Unity meshes can only have up to 65534 vertices, because they
  // use 16-bit index buffers, and I guess -1 and -2 are special values.
  const int kPointsPerChunk = 65534;

  static System.Reflection.FieldInfo FIELD_ByteString_bytes =
    typeof(Google.ProtocolBuffers.ByteString).GetField("bytes",
             System.Reflection.BindingFlags.NonPublic |
             System.Reflection.BindingFlags.GetField |
             System.Reflection.BindingFlags.Instance);

  // Useful topics:
  //  "/hangar_03/wall/lasers/velodyne_01/velodyne_points",
  //  "/hangar_03/wall/lasers/velodyne_02/velodyne_points",
  //  "/hangar_03/wall/lasers/merged/merged/velodyne_points",
  //  "/voxel_filter",
  //  "/husky/lasers/velodyne_01/velodyne_points",
  public PointCloudClient(string topic)
    : base(topic, "sensor_msgs/PointCloud2") {}

  // Copies the Vec3 data in the range [start_item, end_item) to output[].
  // 
  // input[] is an array of items (Vec3s), with the passed stride.
  // start_item and end_item are item indices (not byte indices)
  //
  // This is very fast if the input data is packed (ie, stride 12).
  // It is less fast if the data needs to be packed from another stride.
  protected static unsafe void Convert(
      byte[] input, int stride, int start_item, int end_item,
      out Vector3[] output) {
    int num_items = end_item - start_item;
    output = new Vector3[num_items];
    fixed (Vector3* output_ptr = output) {
      System.IntPtr output_intptr = new System.IntPtr(output_ptr);
      if (stride == sizeof(Vector3)) {
        System.Runtime.InteropServices.Marshal.Copy(
            input, start_item * stride, output_intptr, num_items * stride);
      } else {
        fixed (byte* input_ptr = input) {
          unchecked {
            for (int i = 0; i < num_items; ++i) {
              float* point = (float*)(input_ptr + stride * (start_item + i));
              output_ptr[i].x = point[0];
              output_ptr[i].y = point[1];
              output_ptr[i].z = point[2];
            }
          }
        }
      }
    }
  }

  protected override PointsAndTransform processMessage(PointCloud2 msg) {
    Google.ProtocolBuffers.ByteString byteString = msg.Data;
    // byte[] raw = byteString.ToByteArray();   <-- This makes a copy,
    // so use reflection to access the underlying array.
    byte[] raw = (byte[])FIELD_ByteString_bytes.GetValue(byteString);

    PointsAndTransform ret = new PointsAndTransform();
    ret.transform = RosUtils.RosFrameToUnity(msg.Header.FrameId);

    int num_points = (int)msg.Width;
    int num_chunks = (num_points + kPointsPerChunk-1) / kPointsPerChunk;
    int stride = (int)msg.PointStep;

    ret.point_chunks = new Vector3[num_chunks][];
    for (int chunk_idx = 0; chunk_idx < num_chunks; ++chunk_idx) {
      int point_begin = chunk_idx * kPointsPerChunk;
      int point_end = System.Math.Min(point_begin + kPointsPerChunk, num_points);
      Convert(raw, stride, point_begin, point_end, out ret.point_chunks[chunk_idx]);
    }

    return ret;
  }
}

}  // namespace ros
