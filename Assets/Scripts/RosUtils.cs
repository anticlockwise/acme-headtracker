// Helpers for using ROS in Unity

using System.Collections.Generic;
using UnityEngine;

namespace ros
{
public class RosUtils
{
  // Frame used to do the conversions, and conversions into/out of
  // that frame. These matrices are all in ros coordinate conventions.
  private const string kPivotFrame = "/hangar_03";

  // Matrix that converts coordinate and scale conventions
  // from Ros to Unity. This doesn't do any frame translations.
  static public readonly Matrix4x4 kRosToUnity;

  static private Dictionary<string, Matrix4x4> toPivot_;
  static private Dictionary<string, Matrix4x4> fromPivot_;

  // Data used for final conversion from Ros to Unity conventions.
  static private float metersPerUnityUnit_;

  // Helper: Fills in a missing Matrix4x4 constructor.
  static private Matrix4x4 FromAxes(
      Vector3 x, Vector3 y, Vector3 z, Vector3 t) {
    Matrix4x4 ret = new Matrix4x4();
    ret.SetColumn(0, new Vector4(x.x, x.y, x.z, 0));
    ret.SetColumn(1, new Vector4(y.x, y.y, y.z, 0));
    ret.SetColumn(2, new Vector4(z.x, z.y, z.z, 0));
    ret.SetColumn(3, new Vector4(t.x, t.y, t.z, 1));
    return ret;
  }

  static RosUtils() {
    // ROS uses   forward=+x, right=-y, up=+z
    // Unity uses forward=+z, right=+x, up=+y (nb: left-handed!)
    // ROS uses meters; also scale to unity units
    float s = 1f / UnityConfig.kMetersPerUnityUnit;
    kRosToUnity = FromAxes(
        new Vector3( 0, 0, s), // ros x => unity z
        new Vector3(-s, 0, 0), // ros y => unity -x
        new Vector3( 0, s, 0), // ros z => unity y
        new Vector3( 0, 0, 0));

    toPivot_ = new Dictionary<string, Matrix4x4>();
    toPivot_[kPivotFrame] = Matrix4x4.identity;

    // This is a fake tf, centered on the microtiles, with forward
    // axis facing into the tiles
    //   MT X axis -> H3 Y axis
    //   MT Y axis -> H3 -X axis
    toPivot_["<microtile>"] = FromAxes(
        new Vector3( 0,  1,  0),
        new Vector3(-1,  0,  0),
        new Vector3( 0,  0,  1),
        new Vector3(9.26f, 17.27f, 0));

    // These are extracted directly from ROS.
    // TODO(pld): dynamically pluck these out of ROS instead of hardcoding.
    // 
    // To get the matrix that transforms points in frame A to points
    // in frame B, 'rosrun tf tf_echo B A' (nb, this may seem like the
    // arguments to tf_echo are backwards, but it is correct)
    //
    // Alternately, you can read the transforms out of rviz directly;
    // tf_echo gives very low-precision output for some reason.
    toPivot_["/hangar_03/wall/lasers/velodyne_02"] =
      Matrix4x4.TRS(new Vector3(9.30000f, 13.10000f, 2.50000f),
                    new Quaternion(0.0f, 0.0f, -0.76433f, 0.64483f),
                    new Vector3(1,1,1));

    toPivot_["hangar_03/mocap_bridge"] = 
      Matrix4x4.TRS(new Vector3(6.5f, 7.20f, 0.0f),
                    new Quaternion(.69614f, 0.0f, 0.0f, 0.71791f),
                    new Vector3(1,1,1));

    fromPivot_ = new Dictionary<string, Matrix4x4>();

    if (! toPivot_.ContainsKey(UnityConfig.kLocalFrame)) {
      Debug.Log(string.Format("Invalid local frame: {0}",
                              UnityConfig.kLocalFrame));
    }

    foreach (var entry in toPivot_) {
      fromPivot_[entry.Key] = entry.Value.inverse;
    }
  }

  //
  // Public API
  //

  // Gets matrix that converts from ROS coordinate and distance
  // conventions to Unity's.
  static public Matrix4x4 RosToUnity {
    get {
      return kRosToUnity;
    }
  }

  // Gets matrix that converts from Ros sourceFrame to Unity LocalFrame.
  static public Matrix4x4 RosFrameToUnity(string sourceFrame) {
    if (sourceFrame == UnityConfig.kLocalFrame) {
      return kRosToUnity;
    }

    try {
      return (kRosToUnity
              * fromPivot_[UnityConfig.kLocalFrame]
              * toPivot_[sourceFrame]);
    } catch (KeyNotFoundException) {
      Debug.Log(string.Format("Unknown ROS frame: {0}", sourceFrame));
      return kRosToUnity;
    }
  }
}
}  // namespace ros
