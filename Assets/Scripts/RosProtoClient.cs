using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

using ProtoBridgeSetup = ros_proto.proto_bridge.ProtoBridgeSetup;
using UnityEngine;


namespace ros
{
// Generic client for pulling ROS messages off of a socket.
//
// Must be subclassed for each type you want to read. Socket and
// post-processing work is done on a subthread, making it easy to keep
// work off the render thread.
//
// Currently, only the most recent message is buffered.
//
public abstract class RosProtoClient<T, TBuilder, TResult>
  where T : Google.ProtocolBuffers.IMessageLite<T, TBuilder>
  where TBuilder : Google.ProtocolBuffers.IBuilderLite<T, TBuilder>, new() {
  public delegate TBuilder BuilderFactory();

  // private const string kProtoBridgeIp = "172.16.78.10";
  private const string kProtoBridgeHost = "zurg.lab.acme.google.com";
  private const int kProtoBridgePort = 4242;

  private NetworkStream stream_;
  private System.Collections.Generic.Queue<TResult> queue_;
  private BuilderFactory builderFactory_;
  private string topic_, protocolType_;
  private System.Threading.Thread thread_;
  private bool terminateRequested_;
  private TimeSpan rateLimit_ = TimeSpan.Zero;
  private bool disabled_ = false;

  // Pass:
  //  topic         topic string, eg "/zdaa/dj/logitech_c910/image_raw"
  //  protocolType  ros message type, eg "sensor_msgs/Image"
  //
  public RosProtoClient(string topic, string protocolType) {
    queue_ = new System.Collections.Generic.Queue<TResult>();
    builderFactory_ = new TBuilder().DefaultInstanceForType.CreateBuilderForType;
    topic_ = topic;
    protocolType_ = protocolType;
    terminateRequested_ = false;
    stream_ = null;

    if (disabled_) {
      return;
    }

    if (topic_ != null) {
      IPAddress[] addresses;
      try {
        addresses = Dns.GetHostAddresses(kProtoBridgeHost);
        if (addresses.Length == 0) {
          Debug.LogError(string.Format("Unexpected: 0 hosts for {0}", kProtoBridgeHost));
          return;
        }
      } catch (SocketException e) {
        Debug.LogError(string.Format("{0}: Lookup {1} failed\nReason: {2}",
                                     this.GetType().Name, kProtoBridgeHost, e));
        return;
      }

      try {
        Socket socket = new Socket(
            AddressFamily.InterNetwork,
            SocketType.Stream,
            ProtocolType.Tcp);
        socket.Connect(new IPEndPoint(addresses[0], kProtoBridgePort));
        stream_ = new NetworkStream(socket);
      } catch (SocketException e) {
        Debug.LogError(string.Format("{0}: Connect to {1} failed\nReason: {2}",
                                     this.GetType().Name, topic, e));
      }
    }
  }

  // Imposes a minimum time between reading messages from upstream.
  //
  // This method should be used mainly for testing purposes; any real
  // rate-limiting should preferably be done upstream.
  //
  // Rate-limiting at the sink does eventually cause the source to
  // rate-limit, but only after any intervening buffers fill up. The
  // resulting buffer bloat can cause a lot of message latency.
  //
  public void SetRateLimit(double secs) {
    rateLimit_ = TimeSpan.FromSeconds(secs);
  }

  // Kicks off the subthread that reads and processes incoming messages.
  public void Start() {
    if (stream_ != null) {
      thread_ = new System.Threading.Thread(threadMain);
      thread_.IsBackground = true;
      thread_.Start();
    }
  }

  private void setupSubscription(System.IO.Stream stream) {
    ProtoBridgeSetup.Builder b = ProtoBridgeSetup.CreateBuilder();
    b.SetSessionType(ProtoBridgeSetup.Types.SessionType.MSG_SUBSCRIBER);
    b.SetTopic(topic_);
    b.SetProtocolType(protocolType_);
    b.SetQueueSize(1);  // only care about latest messages
    b.SetLatch(false);  // arbitrary -- "latch" is not meaningful for subscribers

    b.BuildPartial().WriteDelimitedTo(stream);
  }

  private void threadMain() {
    setupSubscription(stream_);
    Debug.Log(string.Format("Thread {0}: subscribed {1}",
                            System.Threading.Thread.CurrentThread.ManagedThreadId,
                            topic_));

    var lastMsgTime = DateTime.Now;
    try {
      while (!terminateRequested_) {
        T msg = builderFactory_().MergeDelimitedFrom(stream_).Build();
        TResult result = processMessage(msg);
        lock (this) {
          queue_.Clear();  // Receiver only cares about the most current data
          queue_.Enqueue(result);
        }

        // Sleep a little bit, if the message came in too quickly
        {
          DateTime currentTime = DateTime.Now;
          TimeSpan elapsed = currentTime.Subtract(lastMsgTime);
          lastMsgTime = currentTime;
          if (elapsed < rateLimit_) {
            System.Threading.Thread.Sleep(rateLimit_ - elapsed);
          }
        }
      }
      Debug.Log(string.Format("Thread {0}: exiting cleanly",
                              System.Threading.Thread.CurrentThread.ManagedThreadId));
    } catch (System.Exception e) {
      if (e.InnerException == null ||
          !(e.InnerException is System.Threading.ThreadAbortException)) {
        Debug.Log(string.Format("Thread {0}: exc {1}",
                                System.Threading.Thread.CurrentThread.ManagedThreadId, e));
      }
    }

    stream_.Close();
    stream_ = null;
  }

  // Processes a Ros message into a more convenient application format.
  //
  // Since this is called on the reader thread, it's a good place to
  // do lengthy or expensive transformations that one doesn't want
  // in the render loop.
  //
  protected abstract TResult processMessage(T msg);

  // Fetches the most recently-processed message.
  // Returns true if a message was returned.
  public bool Get(out TResult msg) {
    if (queue_.Count == 0) {
      msg = default(TResult);
      return false;
    }
    lock (this) {
      try {
        msg = queue_.Dequeue();
        return true;
      } catch (InvalidOperationException) {
        msg = default(TResult);
        return false;
      }
    }
  }

  // Requests that the thread terminate itself
  public void Terminate() {
    terminateRequested_ = true;
  }
}
}  // namespace ros
